package vehicle;

public abstract class Vehicle {

    private String color;
    private String name;
    private String serialNr;
    private int model;
    private int price;
    private int direction;
    private double speed = 0;
    protected java.util.Scanner input;

    public Vehicle() {
    }

    public Vehicle(String name, String color, int price, int model, String serialNr, int direction) {
        this.name = name;
        this.color = color;
        this.price = price;
        this.model = model;
        this.serialNr = serialNr;
        this.direction = direction;
    }

    public void setAllFields() {
        System.out.println("Input car data:");
        input.nextLine();

        System.out.println("Name?");
        this.name = input.nextLine();

        System.out.println("Color?");
        this.color = input.nextLine();

        System.out.println("Price?");
        this.price = input.nextInt();

        System.out.println("Model?");
        this.model = input.nextInt();

        System.out.println("Serial?");
        this.serialNr = input.next();
    }

    public abstract void turnLeft(int degrees);

    public abstract void turnRight(int degrees);

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerialNr() {
        return serialNr;
    }

    public void setSerialNr(String serialNr) {
        this.serialNr = serialNr;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public String toString() {
        return "Name: " + getName()
                + "\tColor: " + getColor()
                + "\tPrice: " + getPrice()
                + "\tModel: " + getModel()
                + "\tSerial#: " + getSerialNr()
                + "\tDirection: " + getDirection()
                + "\tSpeed: " + getSpeed();
    }
}
