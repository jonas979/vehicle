package vehicle;

import java.util.Date;

public class Bicycle extends Vehicle {

    private int gears;
    private java.util.Date productionDate;

    public Bicycle() {
    }

    public Bicycle(String name, String color, int price, int model, String serialNr, int direction, int gears) {
        super(name, color, price, model, serialNr, direction);
        this.gears = gears;
        productionDate = new java.util.Date();

    }

    @Override
    public void setAllFields(){
        super.setAllFields();
        System.out.println("Gears?");
        this.gears = input.nextInt();
    }

    @Override
    public void turnRight(int degrees) {
        return;
    }

    @Override
    public void turnLeft(int degrees){
        return;
    }

    public int getGears() {
        return gears;
    }

    public void setGears(int gears) {
        this.gears = gears;
    }

    public Date getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(Date productionDate) {
        this.productionDate = productionDate;
    }

    @Override
    public String toString() {
        return super.toString() + "\tGears: " + getGears() + "\tProduction date: " + getProductionDate();
    }
}
