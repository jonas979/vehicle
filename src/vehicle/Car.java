package vehicle;

import java.util.Date;

public class Car extends Vehicle {

    private int power;
    private java.util.Date productionDate;

    public Car() {
    }

    public Car(String name, String color, int price, int model, String serialNr, int power, int direction) {
        super(name, color, price, model, serialNr, direction);
        this.power = power;
        productionDate = new java.util.Date();
    }

    @Override
    public void setAllFields(){
        super.setAllFields();
        System.out.println("Power?");
        this.power = input.nextInt();
    }

    @Override
    public void turnRight(int degrees) {
        return;
    }

    @Override
    public void turnLeft(int degrees){
        return;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public Date getProductionDate() {
        return productionDate;
    }

    public void setProductionDate(Date productionDate) {
        this.productionDate = productionDate;
    }

    @Override
    public String toString() {
        return super.toString() + "\tPower: " + getPower() + "\tProduction date: " + getProductionDate();
    }
}
